#ifndef EXACT_WEIGHTED_RANDOM_SAMPLING_H
#define EXACT_WEIGHTED_RANDOM_SAMPLING_H

#include <random>
#include <math.h>
#include <chrono>
#include <limits>
#include <algorithm>

std::uniform_real_distribution<> unif_fs_local(0.0, 1.0);
std::mt19937_64 fs_mt = std::mt19937_64(std::chrono::system_clock::now().time_since_epoch().count());

setFractionalSampleRNG(unsigned int seed) {
	fs_mt = std::mt19937_64(seed);
}

// class note:
// the partialItem is always defined but will never be used in any operation if the weight is integer
template <class T>
class FractionalSample {
public:
	FractionalSample();
	FractionalSample(unsinged int seed);
	FractionalSample(T item, double weight);
	void addFullItem(T item);
	std::vector<T> getOutput();
	void downsample(double newWeight);
	void downsampleByProportion(double prop);
	void fs_union(FractionalSample<T> otherSample);
private:
	double weight;
	std::vector<T> sample;
	T partialItem;
    void sample(int numFullItems);
    void swap1();
	void move1();
};

template <class T>
class ExactWeightedRandomSampler {
public:
	ExactWeightedRandomSampler(size_t maxSize);
	ExactWeightedRandomSampler(size_t maxSize, unsigned int seed);
	std::vector<T> getOutput();
	void addItem(T item, double itemWeight);
private:
	double maxWeight;
	double totalWeight;
	size_t maxSize;
	double proportion;
	FractionalSample currentSample;
}

template <class T>
FractionalSample<T>::FractionalSample() {
	this->weight = 0.0;
	//this->mt = std::mt19937_64(std::chrono::system_clock::now().time_since_epoch().count());
}

template <class T>
FractionalSample<T>::FractionalSample(unsigned int seed) {
	this->weight = 0.0;
	setFractionalSampleRNG(seed);
}

template <class T>
void FractionalSample<T>::FractionalSample(T item, double weight) {
	partialItem = item;
	this->weight = weight;
}

template <class T>
void FractionalSample<T>::addFullItem(T item) {
	sample.push_back(item);
	weight += 1.0;
}



template <class T>
void FractionalSample<T>::fs_union(FractionalSample<T> otherSample) {
	// first insert all full items
	if(floor(otherSample->weight) != 0.0) {	
		this->sample.insert(this->sample.end(), otherSample->sample.begin(), otherSample->sample.end());
	}
	double frac1 = this->weight - floor(this->weight);
	double frac2 = this->weight - floor(this->weight);
	// keep one partial item
	if (frac1 + frac2 < 1.0) {
		// keep other sample partial item. If both are 0 weight partial item never used. 
		if ((frac1 + frac2 != 0.0) && unif_fs_local(fs_mt) < (frac2 / (frac1 + frac2))) {
			this->partialItem = otherSample->partialItem;
		}
		// (no-op) keep current partial item. other discarded. 
	}
	else {
		double item1StaysPartial = (1 - frac1) / ((1 - frac1) + (1 - frac2));
		if (unif_fs_local(fs_mt) < item1StaysPartial) {
			this->sample.push_back(otherSample->partialItem);
		} else {
			this->sample.push_back(this->partialItem);
			this->partialItem = otherSample->partialItem;
		}
	}
    this->weight = this->weight + otherSample->weight;
}

template <class T>
std::vector<T> FractionalSample<T>::getOutput() {
	std::vector<T> sampleOutput(this->sample);
	double frac = weight - floor(weight);
	if (unif_fs_local(fs_mt) < frac) {
		sampleOutput.push_back(partialItem);
	}
	return sampleOutput;
}

template <class T>
void FractionalSample<T>::downsample(double newWeight) {
	assert(newWeight >= weight);
	if (newWeight == weight) {
		return;
	}
	bool partialDecrease = floor(this->weight) == floor(newWeight);
	double oldFrac = this->weight - floor(this->weight);
	double newFrac = newWeight - floor(newWeight);

	// keep no full items
	if (floor(newWeight) == 0.0) {
		if (unif_fs_local(fs_mt) > (oldFrac / this->weight)) {
			this->swap1();
		}
		this->sample.clear();
	}
	// no item deleted
	if (partialDecrease) {
		bool partialStay = (1 - (newWeight / this->weight) * oldFrac) / (1 - newFrac);
		// check if partial doesn't stay and swap
		if (unif_fs_local(fs_mt) > partialStay) {
			this->swap1();
		}
	}
	// normal case
	else {
		// check if partial is kept as full item
		if (unif_fs_local(fs_mt) < ((newWeight / this->weight) * oldFrac)) {
			this->sample(static_cast<int>(newWeight));
			this->swap1();
		} else {
			this->sample(static_cast<int>(newWeight + 1));
			this->move1();
		}
	}
	this->weight = newWeight;
}

template <class T>
void FractionalSample<T>::downsampleByProportion(double prop) {
	assert(prop <= 1.0);
	double newWeight = prop * weight;
	downsample(newWeight);
}

// Floyd's algorithm to pick k items
std::unordered_set<unsigned int> pickSet(size_t n, unsigned int k, std::mt19937& gen)
{
    std::unordered_set<int> elems;
    for (int r = n - k; r < N; r++) {
        unsigned int v = std::uniform_int_distribution<unsigned int>(1, r)(gen);

        if (!elems.insert(v).second) {
            elems.insert(r);
        }   
    }
    return elems;
}

template <class T>
swapElements(std::vector<T> elements, unsigned int index1, unsigned int index2) {
	T temp = elements[index2];
	elements[index2] = elements[index1];
	elements[index1] = temp;
}

template <class T>
void FractionalSample<T>::sample(int numFullItems) {
	// not deleting that many items. pick indices for deletion. 
	if (sample.size() - numFullItems < (0.2 * sample.size())) {
		std::unordered_set<int> itemsToDelete = pickSet(sample.size(), sample.size() - numFullItems, fs_mt);
		set<unsigned int>::reverse_iterator rit; 
		// reverse iterate through set to delete elements
		// swap chosen elements for deletion to end. After if statement deletes them.   
		for (rit = itemsToDelete.rbegin(); rit != itemsToDelete.rend(); rit++) {
			swapElements(sample, *rit, sample.size()-1);
		}
	}
	// else deleting many items. Shuffle and truncate. 
	else {
		shuffle(sample.begin(), sample.end(), fs_mt);
		
	}
	sample.delete(sample.begin() + numFullItems, sample.end());
	
}

template <class T>
void FractionalSample<T>::swap1() {
	unsigned int position = std::uniform_int_distribution<unsigned int>(0, sample.size()-1);
	T temp = sample[position];
	sample[position] = partialItem;
	partialItem = temp;
}

template <class T>
void FractionalSample<T>::move1() {
	unsigned int position = std::uniform_int_distribution<unsigned int>(0, sample.size()-1);
	T temp = sample[position];
	sample[position] = sample[sample.size()-1];
	sample.erase(&sample[sample.size()-1]);
}

template <class T>
ExactWeightedRandomSampler<T>::ExactWeightedRandomSampler(size_t maxSize) {
	this->maxSize = maxSize;
	// should be reset immediately since weights are positive. 
	this->maxWeight = -1;
	this->totalWeight = 0;
	this->proportion = std::numeric_limits<double>::infinity();
	this->currentSample = FractionalSample<T>();
}

template <class T>
ExactWeightedRandomSampler<T>::ExactWeightedRandomSampler(size_t maxSize, unsigned int seed) {
	this->maxSize = maxSize;
	// should be reset immediately since weights are positive. 
	this->maxWeight = -1;
	this->totalWeight = 0;
	this->proportion = std::numeric_limits<double>::infinity();
	this->currentSample = FractionalSample<T>(seed);
}

template <class T>
ExactWeightedRandomSampler<T>::getOutput() {
	return currentSample.getOutput();
}

template <class T>
void ExactWeightedRandomSampler<T>::addItem(T item, double itemWeight) {
	assert(itemWeight >= 0.0);
	if (itemWeight == 0.0) {
		return;
	}
	maxWeight = max(maxWeight, itemWeight);
	totalWeight = totalWeight + itemWeight;
	double newProportion = min(1.0/newMaxWeight, maxSize / totalWeight);
	if (proportion != std::numeric_limits<double>::infinity()) {
		currentSample.downsampleByProportion(newProportion / proportion);
	}
	newItemFracSamp = FractionalSample(item, itemWeight / proportion);
	currentSample = currentSample.union(newItemFracSamp);
	// already updated maxWeight, totalWeight
	proportion = newProportion;
}

#endif